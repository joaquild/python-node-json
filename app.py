import sys
import json
import requests

url = "https://api.coinbase.com/v2/exchange-rates?currency=BTC"
r = requests.get(url)
data = r.json()

resp = {
    "Response":200,
    "Message":"Hello From Python File",
    "Data":data}

print(json.dumps(resp))

sys.stdout.flush()