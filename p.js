const http = require('http')
const port = 3000
const spawn = require("child_process").spawn

const pythonProcess = spawn('python3', ["app.py"])

pythonProcess.stdout.on('data', (data) =>{
    // do something with data

    //convert string to Json
    mystr = data.toString()
    myjson = JSON.parse(mystr);
    console.log('Json IS: ${myjson}');
    console.log(myjson.Data)
    const message = JSON.stringify(myjson)
    console.log('mensaje: '+ message)  
})

const server = http.createServer(function (req, res){
    res.setHeader("Content-type", "application/json")
    res.writeHead(200)
    res.end()
})

server.listen(port, function(error) {
    if (error){
        console.log('Someting wnet wrong', error)}
    else{
        console.log('Server is listening on port: ' + port)}
})